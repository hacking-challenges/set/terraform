#!/bin/bash

update() {
    echo "Updating apt"
    apt-get update -y
}

create_user() {
    echo "Adding $1"
    useradd -m -s /bin/bash -G sudo "$1"
    echo "$1:$2" | chpasswd
}

configure_ssh() {
    echo "Configuring ssh"
    sed -i -e 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
    systemctl restart sshd.service
}


install_set() {
    git clone https://github.com/trustedsec/social-engineer-toolkit/ setoolkit/
    pip install -r /setoolkit/requirements.txt
    python /setoolkit/setup.py
}

update

create_user llama carlthatkillspeople

configure_ssh

install_set
