provider "cloudflare" {}
provider "digitalocean" {}

locals {
  name = "set"
}

variable "cloudflare_zone_id" {}

variable "firewall_allowed_ips" {
  default = []
}

module "server" {
  source = "git@gitlab.com:hacking-challenges/terraform-modules/droplet.git?ref=v0.0.3"
  name   = local.name
  script = file("./scripts/setup.sh")
  tags   = [local.name]
}

module "firewall" {
  source               = "git@gitlab.com:hacking-challenges/terraform-modules/firewall.git?ref=v0.0.1"
  name                 = local.name
  droplet_ids          = [module.server.id]
  firewall_allowed_ips = var.firewall_allowed_ips
}

resource "cloudflare_record" "this" {
  zone_id = var.cloudflare_zone_id
  name    = "llama.hacking"
  value   = module.server.ip
  type    = "A"
}

output "ip" {
  value = module.server.ip
}

output "url" {
  value = cloudflare_record.this.hostname
}
